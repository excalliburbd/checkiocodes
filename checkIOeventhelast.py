def checkio(array):
    """
        sums even-indexes elements and multiply at the last
    """
    new_array = []
    counter = 0
    if len(array) == 0:
        return 0
    for instance in array:
        if counter%2 == 0:
            new_array.append(instance)
        counter = counter + 1
    
    ans = 0
    for instance in new_array:
        ans = ans + instance
    
    ans = ans * array[len(array)-1]
    return ans

#These "asserts" using only for self-checking and not necessary for auto-testing
if __name__ == '__main__':
    assert checkio([0, 1, 2, 3, 4, 5]) == 30, "(0+2+4)*5=30"
    assert checkio([1, 3, 5]) == 30, "(1+5)*5=30"
    assert checkio([6]) == 36, "(6)*6=36"
    assert checkio([]) == 0, "An empty array = 0"
