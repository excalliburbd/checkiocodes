def digit_stack(commands):
    stack = []
    stack_sum = []
    final_sum = 0
    for instance in commands:
        if instance[:4] == "PUSH":
            stack.append(instance[5:])
        elif instance[:3] == "POP":
            try:
                stack_sum.append(stack.pop())
            except:
                stack_sum.append("0")
        elif instance[:4] == "PEEK":
            stack_sum.append(stack[len(stack)-1])

    for number_string in stack_sum:
        if number_string == "0":
            final_sum = final_sum + 0
        elif number_string == "1":
            final_sum = final_sum + 1
        elif number_string == "2":
            final_sum = final_sum + 2
        elif number_string == "3":
            final_sum = final_sum + 3
        elif number_string == "4":
            final_sum = final_sum + 4
        elif number_string == "5":
            final_sum = final_sum + 5
        elif number_string == "6":
            final_sum = final_sum + 6
        elif number_string == "7":
            final_sum = final_sum + 7
        elif number_string == "8":
            final_sum = final_sum + 8
        elif number_string == "9":
            final_sum = final_sum + 9

    print final_sum

    return final_sum

if __name__ == '__main__':
    #These "asserts" using only for self-checking and not necessary for auto-testing
    assert digit_stack(["PUSH 3", "POP", "POP", "PUSH 4", "PEEK",
                        "PUSH 9", "PUSH 0", "PEEK", "POP", "PUSH 1", "PEEK"]) == 8, "Example"
    assert digit_stack(["POP", "POP"]) == 0, "pop, pop, zero"
    assert digit_stack(["PUSH 9", "PUSH 9", "POP"]) == 9, "Push the button"
    assert digit_stack([]) == 0, "Nothing"
